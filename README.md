## Software Engineering, ML and Systems

### About me

- Software Engineer, ML Engineer, Data Scientist, Writer, Electrical Engineer, and forever learner
- With years of experience in engineering, I write DS and AI software applications to make systems smarter
- Feel free to reach me at timothy@ourse.co

**Languages and Tools:**  

<img align="left" width="30px" src="https://img.icons8.com/color/48/000000/python--v1.png"/>

<img align="left" width="30px" src="https://img.icons8.com/color/48/000000/c-programming.png"/>

<img align="left" width="30px" src="https://img.icons8.com/color/48/000000/c-plus-plus-logo.png"/>

<img align="left" width="30px" src="https://img.icons8.com/color/48/000000/tensorflow.png"/>

<img align="left" width="26px" src="https://upload.wikimedia.org/wikipedia/commons/1/10/PyTorch_logo_icon.svg"/>

<img align="left" width="30px" src="https://upload.wikimedia.org/wikipedia/commons/3/38/Jupyter_logo.svg"/>

<br></br>
